import cv2
import os
import json
import shutil  
import numpy as np 
mask_size = [600, 800]


def main(data_in_folder, data_out_folder):

	for filename in os.listdir(data_in_folder):
		image_id = filename.split('.')[0]
		folder_per_image_id = data_out_folder + image_id + "/"
		images_per_image_id = folder_per_image_id + "images/"
		print("images_per_Image_Id", images_per_image_id)
		mask_per_image_id = folder_per_image_id + "masks/"
		print("mask_per_Image_Id", mask_per_image_id)

		for folder in [folder_per_image_id, images_per_image_id, mask_per_image_id]:
			if not os.path.exists(folder):
				os.makedirs(folder)

		if filename.endswith("json"):
			print("Label file", data_in_folder+filename)
			with open(data_in_folder+filename) as f:
				label_file = json.load(f)

			save_mask_filename = 0
			for each_contour in label_file["shapes"]:
				mask = np.zeros(mask_size)
				save_mask_filename = save_mask_filename + 1
				str_save_mask_filename = str(save_mask_filename)
				# arr = ast.literal_eval(each_contour)
				arr = each_contour["points"]
				arr = np.array(arr, dtype=np.int32)
				cv2.fillPoly(mask, [arr], color=(255))
				save_mask_filename_full = mask_per_image_id + str_save_mask_filename + filename.split('.')[0]+'.jpg'
				cv2.imwrite(save_mask_filename_full, mask)

		elif filename.endswith("jpg"):
			print("image file", filename)

			source_image_path = data_in_folder + filename
			destination_image_path = images_per_image_id + filename
			shutil.copyfile(source_image_path, destination_image_path)


if __name__ == '__main__':

	for component in ['Contact pattern', 'Flank']:
		for data_type in ['training', 'validation']:
			data_in_folder = '../data/as_received/'+component+'/Coast-side/'+data_type+'/'
			data_out_folder = '../data/'+component+'/training/'
			main(data_in_folder, data_out_folder)