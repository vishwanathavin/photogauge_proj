import numpy as np
import trimesh


def plot(x_axis, y_axis, plot_entity):
    # TODO check if the values of cs_contours are ok
    # TODO Check if the plot_entity has the acceptable value
    import matplotlib.pyplot as plt
    plt.style.use('seaborn-whitegrid')

    fig = plt.figure()
    ax = plt.axes()
    plt.title(plot_entity + " of CS along axis")
    plt.xlabel("Axis location")
    plt.ylabel(plot_entity + " of Cross Section")
    plt.plot(x_axis, y_axis)
    plt.show()

def load_mesh(path):
    return trimesh.load_mesh(path)