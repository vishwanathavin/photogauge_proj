# Inspired from: https://stackoverflow.com/questions/39741163/delaunay-triangulation-of-point-cloud
import numpy as np
# import meshio
from stl import mesh

num_u_pts = 30
num_v_pts = 24
width = 5


class Surface():
    def __init__(self):
        self.u_range, self.v_range, self.rad_values = self.gen_param_values()

    def gen_param_values(self):
        u_range = np.linspace(0.0, 2 * np.pi, num=num_u_pts, endpoint=True)
        v_range = np.linspace(-0.5 * width, 0.5 * width, num=num_v_pts)
        radius_list = np.linspace(2.5, 5, num=num_v_pts)
        return u_range, v_range, radius_list

    @property
    def get_surface_pts(self):
        nodes = []
        _ = [nodes.extend(self.get_cs_curve(index, v)) for index, v in enumerate(self.v_range)]
        return np.array(nodes)

    def get_cs_curve(self, vindex, v):
        cs_pts = []
        for uindex in range(len(self.u_range)):
            if self.u_range[uindex] == 0.0:
                u = self.u_range[uindex+1]
                groove_depth = self.rad_values[vindex] * 0.2
            elif self.u_range[uindex] == 2*np.pi:
                u = self.u_range[uindex - 1]
                groove_depth = self.rad_values[vindex] * 0.2
            else:
                u = self.u_range[uindex]
                groove_depth = 0.0
            cs_pts.append([self.rad_values[vindex] * np.cos(u) - groove_depth,
                           self.rad_values[vindex] * np.sin(u),
                           v])
        return cs_pts

    @property
    def get_suface_faces(self):
        elems = []
        _ = [elems.extend([[j * num_u_pts + i, (j + 1) * num_u_pts + i + 1, j * num_u_pts + i + 1],
                           [j * num_u_pts + i, (j + 1) * num_u_pts + i, (j + 1) * num_u_pts + i + 1]])
             for j in range(num_v_pts - 1) for i in range(num_u_pts - 1)]

        # CLose the geometry
        for j in range(num_v_pts - 1):
            elems.append([j*num_u_pts, (j+1)*num_u_pts-1, (j + 1)*num_u_pts])
            elems.append([(j+1)*(num_u_pts) -1, (j + 1)*num_u_pts, (j+2)*(num_u_pts)-1])

        return np.array(elems)

    def write_mesh(self, path):
        # meshio.write_points_cells(path, self.get_surface_pts, {"triangle": self.get_suface_faces})
        faces = self.get_suface_faces
        vertices = self.get_surface_pts
        model = mesh.Mesh(np.zeros(faces.shape[0], dtype=mesh.Mesh.dtype))
        for i, f in enumerate(faces):
            for j in range(3):
                model.vectors[i][j] = vertices[f[j], :]
        model.update_normals()
        # Write the mesh to file "cube.stl"
        model.save(path)
        return


def main():
    surface = Surface()
    surface.write_mesh('/media/vizav/D2642B84642B6A85/PhotoGauge/src/helper_tools/debug_data/mesh.stl')


if __name__ == '__main__':
    main()
