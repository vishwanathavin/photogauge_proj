import trimesh
import numpy as np
from trimesh.triangles import bounds_tree
vertices = np.array([[0, 0, 0], [1, 0, 0], [1, 1, 0], [0, 1, 0]])
faces = [[0, 1, 2], [0, 2, 3]]

mesh = trimesh.Trimesh(vertices=vertices, faces=faces)
tree = mesh.triangles_tree
print(tree)