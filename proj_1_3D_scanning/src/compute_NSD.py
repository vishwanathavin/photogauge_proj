import utils
from tiremesh import TireMesh


def main(args):
    # Read mesh file
    mesh = utils.load_mesh(args['inp_file_path'])

    # Initialize mesh object
    tire_mesh = TireMesh(mesh=mesh)

    groove_depths, vertices = zip(*[tire_mesh.get_groove_depth(face) for face in args["face_list"]])

    print(groove_depths, vertices)


if __name__ == '__main__':
    inp_file_path = '../data/dummpy_mesh.stl'
    face_list = [1351, 1375]

    args = {
        "inp_file_path": inp_file_path,
        "face_list": face_list
    }

    main(args)