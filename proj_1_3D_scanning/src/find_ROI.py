from tiremesh import TireMesh
import utils
import sys

is_plot = False
save_cs = False
debug = True


def main(args):

    # Read mesh file
    mesh = utils.load_mesh(args['inp_file_path'])

    # Initialize mesh object
    tire_mesh = TireMesh(axis=args["inp_axis"], mesh=mesh)

    if debug:
        tire_mesh.get_cross_section_props()
        x_axis, y_axis = tire_mesh.plot_params
        utils.plot(x_axis, y_axis, 'length')

    # Get Region of interest
    trimmed_mesh = tire_mesh.get_region_of_interest()

    # Save new_mesh
    trimmed_mesh.export(args["trimmed_model_path"])


if __name__ == '__main__':
    
    inp_file_path = '../data/scan_STL.stl'
    inp_axis = (1, 0, 0)
    trimmed_model_path = '../data/trimmed_STL.stl'

    args = {
        "inp_file_path": inp_file_path,
        "trimmed_model_path": trimmed_model_path,
        "inp_axis": inp_axis
    }

    main(args)
