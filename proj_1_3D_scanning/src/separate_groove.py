import utils
from tiremesh import TireMesh


def main(args):
    # Read mesh file
    mesh = utils.load_mesh(args['inp_file_path'])
    convex_mesh = mesh.convex_hull
    convex_mesh.export(args['out_file_path'])

    # groove_mesh = convex_mesh.difference(mesh)


if __name__ == '__main__':
    inp_file_path = '../data/dummpy_mesh.stl'
    out_file_path = '../data/dummpy_mesh_convex.stl'
    args = {
        "inp_file_path": inp_file_path,
        "out_file_path": out_file_path,
    }

    main(args)