import trimesh
import numpy as np
sampling_locations = 4
adjacency_depth = 6


class TireMesh(trimesh.Trimesh):
    def __init__(self, mesh, axis=(1, 0, 0)):
        super().__init__(vertices=mesh.vertices, faces=mesh.faces, face_normals=mesh.face_normals)
        self.axis = np.array(axis)
        self.origin = np.array(self.bounds[0])

    def export_mesh(self, path):
        self.export_mesh(path)

    def get_cross_section_props(self):
        self.cs_contours = self.section_multiplane(plane_origin=self.origin,
                                                   plane_normal=self.axis,
                                                   heights=self.get_sampling_heights)

    @property
    def get_sampling_heights(self):
        return np.linspace(0, self.bounds[1][self.axis.nonzero()]
                           - self.bounds[0][self.axis.nonzero()],
                           sampling_locations)

    def get_lowest_index(self, param='length'):
        # lowest_length_index, _ = min(enumerate(self.cs_contours), key=operator.itemgetter(1))
        lowest_length_index = min(range(len(self.cs_contours)), key=lambda index: self.cs_contours[index].length if self.cs_contours[index] is not None else np.inf)
        return lowest_length_index

    def transformed_plane_origin(self, height):
        return self.origin + height*self.axis

    @property
    def trim_plane_origin_normal(self):
        return self.axis

    @property
    def plot_params(self):
        return zip(*[(self.transformed_plane_origin(height)[self.axis.nonzero()], cs.length)
                     for height, cs in zip(self.get_sampling_heights, self.cs_contours) if cs is not None])

    def get_region_of_interest(self):

        # Get the CS sections
        # self.get_cross_section_props()

        # identify filter condition
        height = self.get_sampling_heights[self.get_lowest_index()]

        # Trim the mesh
        new_mesh = self.slice_plane(plane_origin=self.transformed_plane_origin(height), plane_normal=-self.axis)
        return new_mesh

    def get_adjacent_vertices(self, depth, root_vertex):
        vertex_list = [root_vertex]
        index = 0
        for level in range(depth):
            for vertex in vertex_list[index:]:
                for neighbour in self.vertex_neighbors[vertex]:
                    if neighbour not in vertex_list:
                        vertex_list.append(neighbour)
                index += 1
        return vertex_list

    def get_groove_depth(self, face_id):
        root_vertex = self.faces[face_id][0]
        adj_vertex_list = self.get_adjacent_vertices(adjacency_depth, root_vertex)
        face_vertex_dist_list = trimesh.points.point_plane_distance(self.vertices[adj_vertex_list],
                                                                    self.face_normals[face_id],
                                                                    self.vertices[root_vertex])

        depth, vertex = np.max(face_vertex_dist_list), adj_vertex_list[np.argmax(face_vertex_dist_list)]

        return depth, vertex
